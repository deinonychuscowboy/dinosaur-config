Applications/packages
- Telegram
- quodlibet

Bugs
- Ilokelesia needs pci=nomsi,noaer added to kernel line
- Hypsilophodon needs i8042.notimeout i8042.nomux
- Handle session autolaunch config and first time per-user stuff somehow
- cronjobs: backups for acrotholus, docker container prune -f && docker image prune -fa for docker-workstation
- Hypsilophodon needs xserver-xorg-input-libinput removed and -evdev and -synaptics installed for 20.10 and later, clickpad does not work properly with libinput

