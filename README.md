Dinosaur Config
===============

This is my personal config setup for using my `dinosaur` image-builder utility. I use this config to generate both VirtualBox virtual machines and partition images for my physical systems, with all my package customizations and config preapplied so I don't have to spend hours setting up everything after I do a reinstall. Instead, I just spent many more hours programming this application and writing this config and testing it over and over. You know how it is.

The Dinosaur repo provides a better documentation of how the system works overall.

A couple included elements of note:

* The maintenance template builds a small-as-possible image with the necessary components to boot into a GUI and do partition stuff in case your main system is hosed. Just right-click after you log in to bring up the openbox menu.
* The "Chicago" project builds a close replica of the Windows 9x interface and user experience, using the Chicago95 project for xfce. It's not pixel-for-pixel identical, but it's close enough that it "feels kinda like windows" even when you're doing things like using the terminal where the unixyness is exposed. And, unlike Windows 9x, it's safe to connect to the internet in the 2020s!

Resources
---------

* A large assortment of autoinstall configurations demonstrating how to do various things with ubuntu's new autoinstall system that replaced debian preseeding in 20.04
* An assortment of configuration scripts, some of which do fancy things like apply config to users' dbus-dependent config stores (dconf and xfconf) in a way that works even on the command line
* An assortment of application installs for software that isn't in the ubuntu package repositories or needs special handling

Templates
---------

These are a further refinement of the target system to specify all the variants and targets that are needed together for a specific use case. They're just bash scripts.

Variants
--------

These are mostly lists of package sets and install scripts.
